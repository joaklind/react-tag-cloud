# Custom react component for creating a tag cloud
This is an experiment in creating a react component for generating a tag cloud

The backend has a tag repository implementation that fetch a repository from github and returns a list of tags and it's frequncy. 
The repository and number of words can be updated by setting in input fields. The update is driven by react-redux and thunk middleware.

## Requirements
This project requires:
* npm
* java 8

## Run
Do the following to run the application:

First build js resources
```bash
cd src/main/resources/static
npm install
npm run build

```

Then start the boot application. Either run the main application class in your favourite IDE or run the following comand in the root dir

```bash
./gradlew bootRun
```

Then navigate to http://localhost:8080 and press update to generate the first cloud

## Issues
I started out trying to use d3-cloud to generate the word cloud. However this turned out to be a bad match because d3 works by modifying the DOM which does not go very well with Reacts virtualDOM. A better way would probably be to generate the SVG.

## Improvements
* Add more implementations of tagRepository
* Figure out a way to get webpack watch to work
* Add swagger or raml for api documentation
* Add choice for font in UI
