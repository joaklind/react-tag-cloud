import cloud from "d3-cloud"
import * as d3 from "d3"
import ReactFauxDOM from "react-faux-dom"
import React, { PropTypes } from 'react'

const fill = d3.scaleOrdinal(d3.schemeCategory20);

//TODO: This turned into a ugly hack. d3 modifies DOM. Not a good match with React and adding Faux dom is not a good solution. Create standalone svg instead!
export default class Cloud extends React.Component {
    static propTypes = {
        repo: React.PropTypes.string,
        words: React.PropTypes.number,
        items: React.PropTypes.array
    };

    render() {
        this.wordCloud = ReactFauxDOM.createElement('div');

        var layout = cloud()
            .size([500, 500])
            .words(this.props.items)
            .padding(5)
            .rotate(function () {
                return ~~(Math.random() * 2) * 90;
            })
            .font("Impact")
            .fontSize(function (d) {
                return d.size;
            })
            .on('end', words => {
                d3.select(this.wordCloud).append("svg")
                    .attr("width", layout.size()[0])
                    .attr("height", layout.size()[1])
                    .append("g")
                    .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
                    .selectAll("g text")
                    .data(words, function (d) {
                        return d.text;
                    })
                    .enter().append("text")
                    .style("font-size", function (d) {
                        return d.size + "px";
                    })
                    .style("font-family", "Impact")
                    .style("fill", function (d, i) {
                        return fill(i);
                    })
                    .attr("text-anchor", "middle")
                    .attr("transform", function (d) {
                        return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                    })
                    .text(function (d) {
                        return d.text;
                    })
            });

        layout.start();

        return this.wordCloud.toReact();
    }
}


