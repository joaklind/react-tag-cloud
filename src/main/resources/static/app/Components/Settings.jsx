import React, { PropTypes } from 'react'

export default class Settings extends React.Component {

    static propTypes = {
        repo: React.PropTypes.string,
        words: React.PropTypes.number,
        onUpdateClick: React.PropTypes.func
    }

    constructor() {
        super();

        this.handleRepositoryChange = this.handleRepositoryChange.bind(this)
        this.handleWordsChange = this.handleWordsChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

    }

    handleRepositoryChange(event) {
        this.setState({
            repo: event.target.value
        })
    }

    handleWordsChange(event) {
        this.setState({
            words: event.target.value
        })
    }

    handleSubmit(event) {
        this.props.onUpdateClick(this.state.repo, this.state.words)
        event.preventDefault()
    }

    componentWillMount() {
        this.state = {
            repo: this.props.repo,
            words: this.props.words
        }
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    GitHub Repository:
                    <input type="text" value={this.state.repo} onChange={this.handleRepositoryChange}/>
                </label>
                <label>
                    Words:
                    <input type="text" value={this.state.words} onChange={this.handleWordsChange}/>
                </label>
                <input type="submit" value="Update"/>
            </form>
        )
    }
}