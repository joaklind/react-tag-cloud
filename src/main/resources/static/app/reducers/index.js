const cloudApp = (state = {repo: "facebook/react", words: 10, items: [] }, action) => {
    switch (action.type) {
        case "UPDATE_CLOUD":
            return {
                ...state,
                items: action.items
            }
        default:
            return state
    }
}

export default cloudApp