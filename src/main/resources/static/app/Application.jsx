import React from "react";

import WordCloud from "./Containers/WordCloud.jsx"
import SettingsForm from "./Containers/SettingsForm.jsx"

export default class App extends React.Component {

    render() {
        return (
            <div>
                <h1 className="center">React tag cloud experiment</h1>
                <SettingsForm/>
                <WordCloud/>
            </div>
        );
    }
}

