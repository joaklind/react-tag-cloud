import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux"
import { createStore, applyMiddleware} from "redux"
import thunkMiddleware from 'redux-thunk'
import reducer from './reducers'

import App from "./Application.jsx"

class Main extends React.Component {

    render() {
        let store = createStore(
            reducer,
            applyMiddleware(thunkMiddleware)
        );

        return (
            <Provider store={ store }>
                <App/>
            </Provider>
        );
    }
}

ReactDOM.render(<Main/>, document.getElementById("app"));
