import {connect} from 'react-redux'
import Cloud from '../Components/Cloud.jsx'

const mapStateToProps = (state) => ({
    ...state
})

const WordCloud = connect(
    mapStateToProps,
)(Cloud)

export default WordCloud


