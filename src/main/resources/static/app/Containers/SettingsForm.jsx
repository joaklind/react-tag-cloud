import { connect } from 'react-redux'
import { fetchTags } from '../actions'
import Settings from '../Components/Settings.jsx'


const mapStateToProps = (state) => ({
    repo: state.repo,
    words: state.words
})

const mapDispatchToProps =  ({
    onUpdateClick: fetchTags
})

const SettingsForm = connect(
    mapStateToProps,
    mapDispatchToProps
)(Settings)

export default SettingsForm
