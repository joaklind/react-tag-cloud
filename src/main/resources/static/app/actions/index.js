function updateTags(items) {
    return {
        type: "UPDATE_CLOUD",
        items: items
    }
}

function fetchingTags() {
    return {
        type: "FETCHING_TAGS"
    }
}

export const fetchTags = (repository, words) => {
    return function(dispatch) {
        dispatch(fetchingTags())

        fetch("http://localhost:8080/api/tag?query=" + repository)
            .then(response => {
                return response.json();
            })
            .then(json => {
                return json.sort((a,b) => b.frequency - a.frequency).slice(0, words).map(item => {return {text: item.tag, size: item.frequency}});
            })
            .then(items => {
                dispatch(updateTags(items))
            });
    }
}

