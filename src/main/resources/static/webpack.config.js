const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    "cache": true,
    "devtool": "inline-source-map",
    "entry": "./app/main.jsx",
    "output": {
        "path": __dirname,
        "filename": "cloud.js",
        "sourceMapFilename": "[file].map"
    },
    "module": {
        "loaders": [{
            "test": /.jsx?$/,
            "loader": "babel-loader",
            "exclude": /node_modules/,
            "query": {
                "presets": ["es2015", "react", "stage-2"]
            }
        }]
    },
    "plugins": [
        new ExtractTextPlugin("main.css")
    ]
};