package se.joaklind.infrastructure.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Issue {

	private final String body;
	
	public Issue(final @JsonProperty("body") String body) {
		this.body = body;
	}
	
	public String body() {
		return body;
	}
	
}
