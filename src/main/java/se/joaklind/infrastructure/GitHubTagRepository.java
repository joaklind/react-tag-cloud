package se.joaklind.infrastructure;

import org.apache.commons.lang3.Validate;
import org.springframework.web.client.RestTemplate;
import se.joaklind.domain.Frequency;
import se.joaklind.domain.Tag;
import se.joaklind.domain.TagRepository;
import se.joaklind.domain.Word;
import se.joaklind.infrastructure.dto.Issue;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static org.apache.commons.lang3.Validate.notNull;

/**
 * Github implementation of the domain interface TagRepository
 * @see TagRepository
 */
public class GitHubTagRepository implements TagRepository{

	private final RestTemplate gitRestTemplate;
	
	public GitHubTagRepository(final RestTemplate restTemplate) {
		this.gitRestTemplate = Validate.notNull(restTemplate);
	}
	
	@Override
	public Set<Tag> tags(String query) {
		return translate(splitAndSort(issues(notNull(query))));
	}
	
	private List<Issue> issues(final String query) {
		try {
			return Arrays.asList(gitRestTemplate.getForObject(String.format("/repos/%s/issues", query), Issue[].class));
		} catch(final Exception e) {
			//Return empty list on errors for resillience
			return emptyList();
		}
	}
	
	private Map<String, Long> splitAndSort(final List<Issue> issues) {
		return issues.stream()
				.map(Issue::body)
				.map(String::toLowerCase)
				.map(line -> line.split("\\s+"))
				.flatMap(Arrays::stream)
				.collect(groupingBy(i -> i,counting()));
	}
	
	private Set<Tag> translate(final Map<String, Long> sortedIssues) {
		return sortedIssues.entrySet().stream()
				.map(e -> new Tag(new Word(e.getKey()), new Frequency(e.getValue())))
				.sorted((a, b) -> Long.compare(b.frequency.value, a.frequency.value))
				.collect(Collectors.toCollection(LinkedHashSet::new));
	}

	
	
}
