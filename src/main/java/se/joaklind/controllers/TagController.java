package se.joaklind.controllers;

import static java.util.stream.Collectors.toList;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import se.joaklind.application.TagService;
import se.joaklind.controllers.dto.Tag;

@RestController
@RequestMapping("/api/tag")
public class TagController {

	@Autowired
	private TagService service;
	
	@RequestMapping(method=RequestMethod.GET)
	public List<Tag> tags(final @RequestParam @NotNull String query) {
		return service.tags(query).stream()
			.map(this::convert)
			.collect(toList());
	}
	
	public Tag convert(final se.joaklind.domain.Tag tag) {
		return new Tag(tag.word.value, tag.frequency.value);
	}
	
	
}