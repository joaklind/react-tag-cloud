package se.joaklind.controllers.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Tag {
	
	private static final String PROPERTY_TAG = "tag";
	private static final String PROPERTY_FREQUENCY = "frequency";
	
	public final String tag;
	
	public final Long frequency;
	
	@JsonCreator
	public Tag(final @JsonProperty(PROPERTY_TAG) String tag, 
			final @JsonProperty(PROPERTY_FREQUENCY) Long frequency) {
		this.tag = tag;
		this.frequency = frequency;
	}
}
