package se.joaklind.domain;

import java.util.Set;

/**
 * Domain interface for tag repository.
 */
public interface TagRepository {
	Set<Tag> tags(final String query);
}
