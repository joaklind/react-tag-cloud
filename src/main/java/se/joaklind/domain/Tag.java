package se.joaklind.domain;

import static org.apache.commons.lang3.Validate.notNull;

public class Tag {
	
	public final Word word;
	public final Frequency frequency;
	
	public Tag(final Word word, final Frequency frequency) {
		this.word = notNull(word);
		this.frequency = notNull(frequency);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((frequency == null) ? 0 : frequency.hashCode());
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (frequency == null) {
			if (other.frequency != null)
				return false;
		} else if (!frequency.equals(other.frequency))
			return false;
		if (word == null) {
			if (other.word != null)
				return false;
		} else if (!word.equals(other.word))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return String.format("Tag { word=%s, frequency=%s }", word, frequency);
	}
}
