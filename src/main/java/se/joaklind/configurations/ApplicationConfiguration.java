package se.joaklind.configurations;

import org.springframework.context.annotation.Configuration;

@EnableTags
@EnableResources
@Configuration
//TODO: Look att adding JWT based authentication
public class ApplicationConfiguration {

}
