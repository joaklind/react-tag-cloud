package se.joaklind.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;
import se.joaklind.domain.TagRepository;
import se.joaklind.infrastructure.GitHubTagRepository;

public class TagConfiguration {

	@Bean
	public TagRepository tagRepository(final GitHubProperties gitHubProperties) {
		return new GitHubTagRepository(new RestTemplateBuilder().rootUri(UriComponentsBuilder.newInstance()
                                                                                             .scheme(gitHubProperties.scheme)
                                                                                             .host(gitHubProperties.host).toUriString()).build());
	}

	@Component
	@ConfigurationProperties(prefix = "tag.github")
	public static class GitHubProperties {

	    public String host;

	    public String scheme = "https";

        public void setHost(final String host) {
            this.host = host;
        }

        public void setScheme(final String scheme) {
            this.scheme = scheme;
        }
    }
}

