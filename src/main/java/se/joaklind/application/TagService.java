package se.joaklind.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.joaklind.domain.Tag;
import se.joaklind.domain.TagRepository;

import java.util.Set;

/**
 * Application service for fetching tags
 *
 */
@Service
public class TagService {

	@Autowired
	private TagRepository repository;
	
	public Set<Tag> tags(final String query) {
		return repository.tags(query);
	}
	
}
