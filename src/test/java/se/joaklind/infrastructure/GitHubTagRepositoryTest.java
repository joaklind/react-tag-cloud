package se.joaklind.infrastructure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.web.client.RestTemplate;

import se.joaklind.domain.Tag;
import se.joaklind.infrastructure.dto.Issue;

public class GitHubTagRepositoryTest {
	
	private static final String VALID_REPOSITORY = "valid/repository";
	private RestTemplate restTemplate = mock(RestTemplate.class);
	
	private GitHubTagRepository repository;
	
	@Test
	public void should_return_tags() {
		givenATagRepository();
		final String query = givenAValidRepositoryWithIssuesContaingWord("body1", "body2", "body3");
		
		final Set<Tag> tags = whenFetchingTags(query);
		
		thenReturnedTagsAreCorrect(tags);
	}
	
	@Test
	public void should_return_tags_with_correct_frequency() {
		givenATagRepository();
		final String query = givenAValidRepositoryWithIssuesContaingWord("Testing word count", "Count with capitals", "This is test Testing count");
		
		Set<Tag> tags = whenFetchingTags(query);
		
		thenReturnedTagsContainWordWithFrequency(tags, "count", 3L);
		thenReturnedTagsContainWordWithFrequency(tags, "testing", 2L);
		
	}
	
	private void givenATagRepository() {
		repository = new GitHubTagRepository(restTemplate);
	}
	
	private String givenAValidRepositoryWithIssuesContaingWord(final String ... words) {
		
		Issue[] issues = Arrays.stream(words)
				.map(Issue::new)
				.toArray(size -> new Issue[size]);
		
		when(restTemplate.getForObject(Mockito.any(String.class), Mockito.any())).thenReturn(issues);
		
		return VALID_REPOSITORY;
	}
	
	private Set<Tag> whenFetchingTags(final String query) {
		return repository.tags(query);
	}
	
	private void thenReturnedTagsAreCorrect(final Set<Tag> tags) {
		tags.stream().forEach(Assert::assertNotNull);
	}
	
	private void thenReturnedTagsContainWordWithFrequency(final Set<Tag> tags, final String word, final Long frequency) {
		Optional<Tag> filteredTag = tags.stream()
			.filter(tag -> tag.word.value.equals(word))
			.reduce((a, b) -> { throw new IllegalArgumentException("Expected to find exactly one word");});
		
		assertTrue("Expected to find word", filteredTag.isPresent());
		assertEquals(word, filteredTag.get().word.value);
		assertEquals(frequency, filteredTag.get().frequency.value);
		
	}
	
}
