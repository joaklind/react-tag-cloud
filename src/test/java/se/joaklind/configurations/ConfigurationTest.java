package se.joaklind.configurations;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import se.joaklind.domain.TagRepository;
import se.joaklind.infrastructure.GitHubTagRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=ApplicationConfiguration.class)
public class ConfigurationTest {

	@Autowired
	private TagRepository tagRepository;
	
	@Test
	public void should_wire_tag_repository() {
		assertTrue(tagRepository instanceof GitHubTagRepository);
	}
	
}
