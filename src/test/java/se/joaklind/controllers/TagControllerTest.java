package se.joaklind.controllers;

import static java.util.Collections.emptySet;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import se.joaklind.application.TagService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TagControllerTest {
	
	private static final String QUERY = "test/query";
	
	@Autowired
	private TestRestTemplate testRestTemplate;
	
	@MockBean
	private TagService tagServiceMock;
	
	@Test
	public void should_call_tag_service() throws Exception {
		given_an_empty_response();
		
		when_fetching_tags();
		
		then_tag_service_is_called();
	}
	
	public void given_an_empty_response() {
		when(tagServiceMock.tags(QUERY)).thenReturn(emptySet());
	}
	
	public void when_fetching_tags() {
		testRestTemplate.getForEntity("/api/tag?query={query}", String.class, QUERY);
	}
	
	public void then_tag_service_is_called() {
		Mockito.verify(tagServiceMock, Mockito.times(1)).tags(QUERY);
	}

}
